package com.me.Benedict_the_Brave;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class UIElement {
	private String name;
	private Rectangle element;
	private TextureRegion texture;
	
	UIElement(String name, TextureRegion texture, float x, float y, float width, float height){
		this.name = name;
		this.texture = texture;
		element = new Rectangle(x,y,width,height);
	}

}
