package com.me.Benedict_the_Brave;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

//

public class GameObject {
	
	public enum State {
		IDLE, WALKING, JUMPING, DYING
	}
	
	private Rectangle object;
	private TextureRegion front;
	private TextureRegion left;
	private TextureRegion right;
	private boolean isLeft, isRight, isJump = false; 
	private State state; 
	private double hp;
	
	//Constructors
	GameObject(){
		object = new Rectangle();
		state = State.IDLE;
		hp = 100;
	}
	
	GameObject(float x, float y){
		object = new Rectangle(x,y,0,0);
		state = State.IDLE;
		hp = 100;
	}

	GameObject(float x, float y, float width, float height){
		object = new Rectangle(x,y,width, height);
		state = State.IDLE;
		hp = 100;
	}
	//Constructors
	
	public TextureRegion getAnimationFrame(){
		if(isLeft){return left;}
		else if(isRight){return right;}
		return front;
	}
	
	//Set textures
	public void setTex(TextureRegion left, TextureRegion right, TextureRegion front){
		this.left = left;
		this.right = right;
		this.front = front;
	}
	
	//Get textures
	public TextureRegion getLeft(){
		return left;
	}
	
	public TextureRegion getRight(){
		return right;
	}

	public TextureRegion getFront(){
		return front;
	}
	
	
	//Getters/Setters
	public void setPos(float x, float y){
		object.x = x;
		object.y = y;
	}
	
	public void setX(float x){
		object.x = x;
	}
	
	public void setY(float y){
		object.y = y;
	}
	
	public float getX(){
		return object.x;
	}
	
	public float getY(){
		return object.y;
	}
	
	public void setWidth(float width){
		object.width = width;
	}
	public void setHeight(float height){
		object.height = height;
	}

	public float getWidth(){
		return object.width;
	}
	
	public float getHeight(){
		return object.height;
	}
	
	public void updateHP(double damage, double healing){
		hp = hp - damage;
		hp = hp + healing;
		updateHP();
	}
	
	public void updateHP(){
		if(hp <= 0){
			isDying();
		}
	}

	//need to pass time from game world
	public void update(){

		
	}
	
	
	//input interface
	public void inputLeft(){
		if(state == State.DYING) return;
		object.x = object.x - (200 * Gdx.graphics.getDeltaTime());
		isLeft = true;
		isRight = false;
		state = State.WALKING;
	}
	
	public void inputRight(){
		if(state == State.DYING) return;
		object.x= object.x + (200 * Gdx.graphics.getDeltaTime());
		isLeft = false;
		isRight = true;
		state = State.WALKING;
		
	}
	
	public void inputSpace(){
		
	}
	
	public void aniIdle(){
		if(state == State.DYING) return;
		isLeft = false;
		isRight = false;
		state = State.WALKING;
	}
	
	public void isDying(){
		state = State.DYING;

	}
	
	
	
}
