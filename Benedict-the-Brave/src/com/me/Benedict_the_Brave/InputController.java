package com.me.Benedict_the_Brave;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
public class InputController {
	
	private OrthographicCamera camera;
	private Vector3 touchPos;
	GameObject player;

	InputController(GameObject player, OrthographicCamera camera){
		this.player = player; 
		this.camera = camera;
		touchPos = new Vector3();
	}
	
	public void update(){
		
		//checks if space bar is pressed, player can jump and move at the same time
		if(Gdx.input.isKeyPressed(Keys.SPACE)){
			player.inputSpace();
		}

		//----------key input----------
		//update left/right, only accepts input from 1 key at a time
		if(Gdx.input.isKeyPressed(Keys.LEFT)){
			player.inputLeft();
		}else if(Gdx.input.isKeyPressed(Keys.RIGHT)){
			player.inputRight();
		}else if(!Gdx.input.isKeyPressed(Keys.SPACE)){
			player.aniIdle();
			}
		if(Gdx.input.isKeyPressed(Keys.A)){
			player.isDying();
		}
		//----------key input----------
		
		//----------touch input----------
		if(Gdx.input.isTouched()){
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			//if touch input is to the left of the player move the player left
			if(touchPos.x < player.getX()){
				player.inputLeft();
			}else {
				//if touch input is to the right of the player move the player right
				player.inputRight();
			}

		}
		//----------touch input----------
		
	}
	
}
	
	
