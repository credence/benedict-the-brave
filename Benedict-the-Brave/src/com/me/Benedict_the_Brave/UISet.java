package com.me.Benedict_the_Brave;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class UISet {
	
	private UIElement[] uiSet;
	private int i;


	
	UISet(){
	uiSet = new UIElement[10];	
	}
	
	public void setElement(String name,TextureRegion texture, float x, float y, float width, float height){
		for(i = 0; i < uiSet.length; i++){
			if(uiSet[i] != null){
				uiSet[i] = new UIElement(name, texture, x, y, width, height);
			}
		}

	}

}
