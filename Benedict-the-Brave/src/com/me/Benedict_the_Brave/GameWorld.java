package com.me.Benedict_the_Brave;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.*;

//keeps track of all elements in the game
//updates all objects when needed
public class GameWorld {

	//initialize game world
	//create player object
	public GameObject player = new GameObject(800/2 - 64/2, 20, 64, 64);
	private TextureAtlas atlas;
	private TextureRegion front;
	private TextureRegion left;
	private TextureRegion right;
	

	GameWorld(TextureAtlas atlas){
		this.atlas = atlas;
		front = atlas.findRegion("forward");
		left  = atlas.findRegion("left1");
		right = atlas.findRegion("right1");
		player.setTex(left, right, front);
		
		
	}
	
	public void update(){
		
	}
	
}
