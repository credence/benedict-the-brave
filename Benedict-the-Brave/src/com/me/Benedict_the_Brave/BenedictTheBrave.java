package com.me.Benedict_the_Brave;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class BenedictTheBrave implements ApplicationListener {
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private GameWorld world;
	private TextureAtlas atlas;
	private InputController input;
	private BitmapFont font;
	private UIManager uiManager;

	
	
	@Override
	public void create() {		

		//texture atlas
		atlas = new TextureAtlas("data/spritesheet");
		
		uiManager = new UIManager();
		
		world = new GameWorld(atlas);
		

		font = new BitmapFont();
		
		//sprite batch
		batch = new SpriteBatch();

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800,480);
		
		input = new InputController(world.player, camera);
		
		
	}

	@Override
	public void dispose() {
//		batch.dispose();
//		texture.dispose();
	}
	

	@Override
	public void render() {		
		//update game world
		input.update();
		world.update();

		//Clear the screen
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		//create sprite batch
//		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		font.setColor(1.0f,1.0f,1.0f,1.0f);
		font.draw(batch, "hello", 400, 300);

		batch.draw(world.player.getAnimationFrame(), world.player.getX(), world.player.getY());
		batch.end();
		
			
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
