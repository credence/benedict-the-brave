package com.me.Benedict_the_Brave;

public class UIManager {

	private UISet startScreen;
	private UISet pauseScreen;
	private UISet gameOverScreen;
	private UISet HUD;

	UIManager(){
		startScreen = new UISet();
		pauseScreen = new UISet();
		gameOverScreen = new UISet();
		HUD = new UISet();
		
	}
}
